import sys
import seabreeze.spectrometers as sb
from msgpack_numpy import patch
import zerorpc


def run(bind='0.0.0.0', port=8000):
    patch()
    access = f"tcp://{bind}:{port}"
    with sb.Spectrometer.from_serial_number() as spec:
        server = zerorpc.Server(spec)
        server.bind(access)
        print(f'Serving on {access}...')
        try:
            server.run()
        except KeyboardInterrupt:
            print('Interrupted.')
        finally:
            server.close()


def main(args=None):
    if args is None:
        args = sys.argv
    if len(args) > 1:
        run(port=int(args[1]))
    else:
        run()


if __name__ == '__main__':
    main()
