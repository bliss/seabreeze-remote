seabreeze-remote
================

Remote access for Ocean Optics spectrometers


Requirements
------------

Get a conda environment:

``` console
$ wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
$ bash Miniconda3-latest-Linux-x86_64.sh -b
$ source ~/miniconda3/bin/activate root
```

Server side
-----------

Install the udev rules and reload:

``` console
$ wget https://github.com/ap--/python-seabreeze/blob/master/misc/10-oceanoptics.rules
$ sudo cp /10-oceanoptics.rules /etc/udev/rules.d/
$ sudo udevadm control --reload-rules
```

Activate the seabreeze environment and run the server:

``` console
$ conda env create -f seabreeze.yml
$ source activate seabreeze
$ python server.py 8888
Serving on tcp://0.0.0.0:8888...
```

Client side
-----------

Active the zerorpc environment and run the client:

``` console
$ conda env create -f zerorpc
$ source activate zerorpc
$ python client.py <server-hostname>:8888
```

Further improvements
--------------------

- Proper argument handling using *argparse*
- Exposing spectrometer attributes through a specific method (e.g. *getattr*)
- Dedicated systemd service ([example](https://github.com/MaxIV-KitsControls/app-maxiv-px5server/blob/master/px5server.service))
- Specific USB rules ([example](https://github.com/MaxIV-KitsControls/app-maxiv-px5server/blob/master/10-usb.rules))
- Automatic configuration (e.g. with an ansible role)
- Or proper packaging (e.g using [fpm](https://github.com/MaxIV-KitsControls/app-maxiv-px5server/blob/master/package.sh))
- Test on a Raspberry Pi
