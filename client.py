import sys
import time
import matplotlib.pyplot as plt
import matplotlib.animation as anim
from msgpack_numpy import patch
from zerorpc import Client


def continuous_plot(spec):
    plt.ion()
    x = spec.wavelengths()
    t = time.time()
    while True:
        y = spec.intensities()
        plt.cla()
        plt.plot(x, y)
        plt.pause(0.001)
        d, t = time.time() - t, time.time()
        print(f'FPS: {1/d:.1f}')


def run(url='localhost:8000'):
    patch()
    spec = Client(f'tcp://{url}')
    continuous_plot(spec)


def main(args=None):
    if args is None:
        args = sys.argv
    if len(args) > 1:
        run(url=args[1])
    else:
        run()


if __name__ == '__main__':
    main()
